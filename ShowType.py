import sublime, sublime_plugin
import re

class TipShowCommand(sublime_plugin.TextCommand):
    def run(self, edit, text=''):
        region = sublime.Region(0, self.view.size())
        self.view.erase(edit, region)
        self.view.insert(edit, 0, text)

class ShowTypeEventListener(sublime_plugin.EventListener):
    def on_selection_modified_async(self, view):
        # view.run_command("show_type")
        return

class ShowTypeCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        view = self.view

        sels = view.sel()
        if not view.match_selector(sels[0].end(), "source.sv"):
            return None
        elif view.settings().get('no_trace', False):
            return None

        word = view.substr(view.word(sels[0].end()))
        if not re.match(r'^[\w_\$]+$', word):
            return None
        
        text = ''
        io_r = self.findIO(view, word, sels[0])
        var_r = self.findBuildIn(view, word, sels[0])

        if not io_r and not var_r:
            sue_r = self.findSUE(view, word, sels[0])
            if not sue_r:
                return None
            else :
                text += self.getText(view, sue_r)
        else :
            text += self.getText(view, io_r)
            text += self.getText(view, var_r)

        self.show(view, text)

    def findDecl(self, view, sel, rexp, hit_scopes):
        decl_r = view.find_all(rexp)
        f_r = []
        for r in decl_r:
            scope_name = view.scope_name(r.begin())
            scope_name_set = set(scope_name.split(" "))
            hit_scope_set = set(hit_scopes)
            if r.end() <= sel.end() or r.contains(sel):
                if scope_name_set & hit_scope_set:
                    f_r.append(r)
            elif r.begin() >= sel.end():
                break
        return f_r

    def findIO(self, view, word, sel):
        decl_re = r'\b(input|output|inout|ref)\b((?:[^;](?!\b(input|output|inout|ref)\b))*?)\b' + word + r'\b';
        hit_scopes = ["meta.variable_decl.sv", "meta.class.ports.sv"]
        return self.findDecl(view, sel, decl_re, hit_scopes)

    def findBuildIn(self, view, word, sel):
        decl_re = r'\b(byte|shortint|int|longint|integer|time|bit|logic|reg|shortreal|real|realtime|string|chandle|event|void|supply0|supply1|tri|triand|trior|trireg|tri0|tri1|uwire|wire|wand|wor|supply0|supply1|tri|triand|trior|trireg|tri0|tri1|uwire|wire|wand|wor)\b((?:[^;](?!\b(byte|shortint|int|longint|integer|time|bit|logic|reg|shortreal|real|realtime|string|chandle|event|void|supply0|supply1|tri|triand|trior|trireg|tri0|tri1|uwire|wire|wand|wor|supply0|supply1|tri|triand|trior|trireg|tri0|tri1|uwire|wire|wand|wor)\b))*?)\b' + word + r'\b';
        hit_scopes = ["meta.variable_decl.sv", "function.parameters.sv"]
        return self.findDecl(view, sel, decl_re, hit_scopes)

    def findSUE(self, view, word, sel):
        decl_re = r'\b' + word + r'\b'
        hit_scopes = ["variable.parameter.struct_union.sv", "variable.parameter.enum.sv"]
        return self.findDecl(view, sel, decl_re, hit_scopes)

    def getText(self, view, regions):
        text = ''
        if regions:
            lines = view.lines(regions[-1])
            for l in lines:
                text += self.getLineNum(view, l) + view.substr(l) + '\n'
        return text

    def getLineNum(self, view, region):
        line_num = view.rowcol(region.begin())[0] + 1
        return "{0:>4}:".format(line_num)

    def show(self, view, text):
        decl_panel = view.window().create_output_panel('decl_panel')
        decl_panel.set_syntax_file("Packages/SystemVerilog/SystemVerilog.tmLanguage")
        decl_panel.settings().set('no_trace', True)
        decl_panel.settings().set('line_numbers', False)
        decl_panel.set_read_only(False)
        decl_panel.run_command('tip_show', {'text': text})
        decl_panel.set_read_only(True)

        view.window().run_command('show_panel', {'panel': 'output.decl_panel'})