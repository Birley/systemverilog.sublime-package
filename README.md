# SystemVerilog Supoort Package for Sublime Text #

![2014-07-31_20-51-29.png](https://bitbucket.org/repo/XjqX4X/images/685389624-2014-07-31_20-51-29.png)
## Features ##
* Syntax highlight for SystemVerilog
* Highlight UVM classes
* Auto completions for UVM classes and UVM macros
* Some other snippets


## Examples ##
### Asynchronous always block ###
![always_example.gif](https://bitbucket.org/repo/XjqX4X/images/1528100869-always_example.gif)
### UVM sequence item ###
![seq_example.gif](https://bitbucket.org/repo/XjqX4X/images/2670397957-seq_example.gif)
### Generate module instance code ###
![instance2.gif](https://bitbucket.org/repo/XjqX4X/images/3354309422-instance2.gif)