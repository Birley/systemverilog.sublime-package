import sublime, sublime_plugin
import os
import sys

__file__ = os.path.normpath(os.path.abspath(__file__))
__path__ = os.path.dirname(__file__)

utility_path = os.path.join(__path__, 'utility')

# if __path__ not in sys.path:
#     sys.path.insert(0, __path__)`

if utility_path not in sys.path:
    sys.path.insert(0, utility_path)

# from InstanceModuleCommand import InstanceModuleCommand
from NewModuleCommand import NewModuleCommand
from InstanceModuleCommand import InstanceModuleCommand
from GenAgentCommand import GenAgentCommand
from GenAgentCommand import GenCfgCommand
from GenAgentCommand import GenSeqItemCommand
from GenAgentCommand import GenSeqCommand
from GenAgentCommand import GenSqrCommand
from GenAgentCommand import GenDrvCommand
from GenAgentCommand import GenMonCommand
from GenAgentCommand import GenAgtCommand
from GenScbdPortCommand import GenScbdPortCommand
from DeclRegWireCommand import DeclRegCommand
from DeclRegWireCommand import DeclWireCommand
from DeclRegWireCommand import DeclRegWireCommand
from PortAlignCommand import PortAlignCommand
from IoAlignCommand import IoAlignCommand
from GenUvmEnvCommand import GenUvmEnvCommand
