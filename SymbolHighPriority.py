import sublime, sublime_plugin
#import re

#rm_head_space_re = re.compile(r'^\s*')

class SymbolHighPriorityEventListener(sublime_plugin.EventListener):
    def on_query_completions(self, view, prefix, locations):
        if not view.match_selector(locations[0], "source.sv"):
            return None
        completions = []
        symbols = view.extract_completions(prefix)
        for symbol in symbols:
            completions.append((symbol,symbol))
        # for (region, symbol) in view.symbols():
        #     symbol = rm_head_space_re.sub('', symbol)
        #     completions.append((symbol,symbol))
        return completions