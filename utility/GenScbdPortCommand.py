import sublime, sublime_plugin
import re
from uvmf.gen_port import gen_port

class GenScbdPortCommand(sublime_plugin.WindowCommand):
    def run(self):
        self.window.show_input_panel("Agent name, Driver TLM Type, Monitor TLM Type:", 
                                     "agt_name, drv_trans, mon_trans", self.on_done, None, None)

    def on_done(self, text):
        args_re = re.compile(r'\s*([a-zA-Z\$_][\w\$_]*)\s*,\s*([a-zA-Z\$_][\w\$_]*)\s*,\s*([a-zA-Z\$_][\w\$_]*)\s*')
        match = args_re.match(text)
        if match:
            cntxt = gen_port(match.group(1), match.group(2), match.group(3))
            v = self.window.new_file()
            v.run_command('insert_snippet', {'contents': cntxt})
            v.set_syntax_file("Packages/SystemVerilog/SystemVerilog.tmLanguage")
