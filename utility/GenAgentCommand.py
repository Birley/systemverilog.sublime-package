import os
import sublime, sublime_plugin
from uvmf.generate import setTitle
from uvmf.generate import genAgtStr
from uvmf.generate import genAgtComp

def set_file_header(view):
    TM_AUTHOR = view.settings().get("author", "<author>")
    TM_EMAIL = view.settings().get("e-mail", "<e-mail>")
    TM_COPYRIGHT = view.settings().get("copyright", "<copyright>")
    setTitle("", TM_AUTHOR, TM_EMAIL, TM_COPYRIGHT)

class GenAgentCommand(sublime_plugin.WindowCommand):
    def run(self):
        set_file_header(self.window.active_view())

        prj_file = self.window.project_file_name()
        if prj_file:
            self.prj_path = os.path.dirname(os.path.normpath(os.path.abspath(prj_file)))
            self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)
        else:
            self.window.show_input_panel("Agent path:", "", self.path_on_done, None, None)

    def name_on_done(self, text):
        self.name = text
        self.agt_path = os.path.join(self.prj_path, self.name) + "_agt"
        if os.path.exists(self.agt_path) and os.path.isdir(self.agt_path):
            pass
        else:
            try:
                os.mkdir(self.agt_path)
            except Exception:
                print("make dir failure")
        genAgtStr(self.name, self.agt_path)

    def path_on_done(self, text):
        self.prj_path = os.path.normpath(os.path.abspath(text))
        if os.path.exists(self.prj_path) and os.path.isdir(self.prj_path) :
            self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)
        else:
            try:
                os.mkdir(self.prj_path)
                self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)
            except Exception:
                print("make dir failure")

class GenAgentComp(sublime_plugin.WindowCommand):
    comp_type = "seq_item"
    def run(self):
        self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)

    def name_on_done(self, text):
        if self.window.active_view().size() == 0:
            view = self.window.active_view()
        else:
            view = self.window.new_file()
        set_file_header(view)

        #command insert has a indent problem, so use clipboard
        clip = sublime.get_clipboard()
        sublime.set_clipboard(genAgtComp(text, self.comp_type))
        view.run_command('paste')
        sublime.set_clipboard(clip)
        view.set_syntax_file("Packages/SystemVerilog/SystemVerilog.tmLanguage")
        view.show(0)

class GenCfgCommand(GenAgentComp):
    comp_type = "cfg"

class GenSeqItemCommand(GenAgentComp):
    comp_type = "seq_item"

class GenSeqCommand(GenAgentComp):
    comp_type = "seq"

class GenSqrCommand(GenAgentComp):
    comp_type = "sqr"

class GenDrvCommand(GenAgentComp):
    comp_type = "drv"

class GenMonCommand(GenAgentComp):
    comp_type = "mon"

class GenAgtCommand(GenAgentComp):
    comp_type = "agt"
