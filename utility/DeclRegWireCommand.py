import sublime, sublime_plugin

def cur_word(view):
	sels = view.sel()
	word = []
	if len(sels) > 0:
		for sel in sels:
			word.append(view.substr(view.word(sel.end())))
		return word
	else:
		return None

def find_decl(view):
	decl_re = r'^\s*(reg|wire|logic)\b.*?[a-zA-Z_\$][\w\$_]*\s*;'
	return view.find_all(decl_re)

def get_decl_pos(view):
	decls = find_decl(view)
	sel = view.sel()[0]
	if len(decls) > 0:
		for decl in decls[::-1]:
			if decl.end() < sel.begin() :
				return view.line(decl).end()
	else:
		decl_flag_end =  view.find('Register/Wire', 0).end()
		if decl_flag_end != -1:
			pos = view.rowcol(decl_flag_end)
			return view.line(view.text_point(pos[0]+1, pos[1])).end()

	return view.line(sel).begin()
	
def gen_decl_line(decl_type, name, width = '1'):
	if width == '' or width.isspace() or width == '1':
		new_decl = decl_type + ' '*(32-len(decl_type)) + name + ';'
	else:
		if width.isdigit():
			width_str = '['+ str(int(width)-1) + ":0]"
		else:
			width_str = '['+ width + "-1:0]"
		new_decl = decl_type + ' '*(8-len(decl_type)) + width_str
		new_decl = new_decl + ' '*(32-len(new_decl)) + name + ';'
	return new_decl

class DeclRegCommand(sublime_plugin.WindowCommand):
	decl_type = 'reg'
	def run(self):
		v = self.window.active_view()
		self.name = cur_word(v)
		if not self.name:
			return

		self.window.show_input_panel("Signal Width:", "", self.width_on_done, None, None)

	def width_on_done(self, text):
		decl = '\n' + gen_decl_line(self.decl_type, self.name[0], text)
		self.window.active_view().run_command('decl_reg_wire', {'decl': decl})

class DeclWireCommand(DeclRegCommand):
	decl_type = 'wire'

class DeclRegWireCommand(sublime_plugin.TextCommand):
	def run(self, edit, decl):
		self.view.insert(edit, get_decl_pos(self.view), decl)
