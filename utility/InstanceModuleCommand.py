import re
import sublime, sublime_plugin

class InstanceModuleCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        view = self.view
        content = view.substr(sublime.Region(0, view.size()))
        self.parse_file(content)

    def parse_file(self, content):
        content = self.remove_comment(content)
        modules = self.find_module(content)
        if modules:
            v = self.view.window().new_file()
            v.set_syntax_file("Packages/SystemVerilog/SystemVerilog.tmLanguage")

        for module in modules:
            module_info = self.parse_module_header(module.group(0))
            if module_info['name']:
                v.run_command('insert', {"characters":module_info['name']+'\n'})
                ports_info = self.parse_port_list(module.group(0), module_info)
                params_info = self.parse_param_list(module.group(0), module_info)
                self.show_ports(v, ports_info)
                self.show_wire(v, ports_info)
                self.show_inst(v, module_info, params_info, ports_info)
            else:
                v.run_command('insert', {"characters": "Can't parse module\n"})
        v.show(0)

    def remove_comment(self, content):
        comment_re = re.compile(r'(//.*?$|/\*.*?\*/)', re.S|re.M)
        content = comment_re.sub('', content)
        return content

    def find_module(self, content):
        module_re = re.compile(r'(\bmodule\b.*?\bendmodule\b)', re.S)
        modules = module_re.finditer(content)
        return modules

    def parse_module_header(self, module):
        module_header_re = re.compile(r"""\bmodule\s+                     # module
                                          (?:(static|automatic)\s+)?      # lifetime
                                          ([a-zA-Z_\$][\w\$_]*)\s*        # module name
                                          (\s*import[^;]*?;)?\s*          # package import declaration
                                          (?:\#\s*\(([^;]*?)\))?\s*       # parameter_port_list
                                          (?:\(([^;]*?)\))?\s*;           # list_of_ports""", re.S|re.X)
        match = module_header_re.search(module)
        if match:
            module_info = dict(zip(['lifetime', 'name', 'imports', 'param', 'port'], match.groups()))
        else:
            module_info = dict.fromkeys(['lifetime', 'name', 'imports', 'param', 'port'], None)
        
        return module_info

    def parse_port_list(self, module, module_info):
        dir_re = re.compile(r'\b(input|output|inout)\b')
        ansi_re = re.compile(r'[a-zA-Z_][\w_]*\s+[a-zA-Z_][\w_]*')
        if (module_info['port'] == None or len(module_info['port']) == 0
            or module_info['port'].isspace() == True):
            ports_info = self.parse_non_ansi_port_list(module)
        else:
            if dir_re.search(module_info['port']) or ansi_re.search(module_info['port']):
                ports_info = self.parse_ansi_port_list(module_info['port'])
            else:
                ports_info = self.parse_non_ansi_port_list(module)
        return ports_info

    def parse_ansi_port_list(self, port_list):
        port_re = re.compile(r"""^\s*([a-zA-Z_][\w_]*)\b\s*.*?  #direction or types
                                     (\[.*?\])?\s*              #packed dimension
                                     ([a-zA-Z_][\w_]*)\s*       #pin name
                                     (\[.*?\])?\s*$             #unpacked dimension """, re.X)
        follow_port_re = re.compile(r'^\s*([a-zA-Z_][\w_]*)\s*(\[.*?\])?\s*$')
        
        ports_info = []
        pre_port_info = None
        ports = port_list.split(',')

        for port in ports:
            match = port_re.match(port)
            if match:
                port_info = dict(zip(['dir_type', 'dim', 'name', 'udim'], match.groups()))
                pre_port_info = dict(port_info)
                ports_info.append(port_info)
                continue

            match = follow_port_re.match(port)
            if match and pre_port_info != None:
                port_info = dict(pre_port_info)
                port_info['name'] = match.group(1)
                port_info['udim'] = match.group(2)
                ports_info.append(port_info)
            else:
                print("Can't Parse Port "+port)

        return ports_info

    def parse_non_ansi_port_list(self, module):
        port_re = re.compile(r'\b((?:input|output|inout)\b[^;]*)')
        ports = port_re.findall(module)
        ports_info = []
        for port in ports:
            ports_info.extend(self.parse_ansi_port_list(port))
        return ports_info

    def parse_param_list(self, module, module_info):
        if module_info['param'] == None:
            ports_info = self.parse_non_ansi_param_list(module)
        else:
            ports_info = self.parse_ansi_param_list(module_info['param'])
        return ports_info

    def parse_ansi_param_list(self, param_list):
        param_re = re.compile(r"""^\s*(parameter|type)?         #keyword
                                   \s*([^=]*\s+)?               #type
                                   ([a-zA-Z_][\w_]*)            #name
                                   (\s*\[.*?\])?                #unpacked array
                                   (?:\s*=\s*(.+)\s*)?$         #value""", re.X)

        params_info = []
        pre_param_info = None
        params = param_list.split(',')
        for param in params:
            match = param_re.match(param)
            if match:
                param_info = dict(zip(['keyword', 'type', 'name', 'udim', 'value'], match.groups()))
                if param_info['keyword'] != None:
                    pre_param_info = dict(param_info)
                else:
                    param_info['keyword'] = pre_param_info['keyword']
                    param_info['type'] = pre_param_info['type']
                    if param_info['value'] == None:
                        param_info['value'] = pre_param_info['value']
                params_info.append(param_info)

        return params_info


    def parse_non_ansi_param_list(self, module):
        param_re = re.compile(r'\b((?:parameter|type)\b[^;]*)')
        params = param_re.findall(module)
        params_info = []
        for param in params:
            params_info.extend(self.parse_ansi_param_list(param))
        return params_info

    def show_ports(self, view, ports_info):
        output = ""
        for i in range(len(ports_info)):
            port = ports_info[i]
            if i != len(ports_info) - 1:
                output += (port['name']+',\n')
            else:
                output += (port['name']+'\n')
        self.buf_print(view, output + '\n\n\n')

    def show_wire(self, view, ports_info):
        for port in ports_info:
            self.padding(view, 'wire', 8)
            if port['dim']:
                self.padding(view, port['dim'], 28)
            else:
                self.padding(view, '', 28)
            self.buf_print(view, port['name']+';\n')
        self.buf_print(view, '\n\n\n')

    def show_inst(self, view, module_info, params_info, ports_info):
        self.buf_print(view, module_info['name'])

        #print parameters
        if len(params_info) > 0:
            self.buf_print(view, ' #(\n    ')
            for i in range(len(params_info)):
                param = params_info[i]
                self.padding(view, '.' + param['name'], 32)
                self.padding(view, '(' + param['value'], 28)
                if i != len(params_info) - 1:
                    self.buf_print(view, '),\n')
                else:
                    self.buf_print(view, ')\n')
            view.run_command('unindent')
            self.buf_print(view, ')')

        #print instance name
        self.buf_print(view, ' u_' + module_info['name'] + ' (\n    ')

        #print ports
        for i in range(len(ports_info)):
            port = ports_info[i]
            if port['dim']:
                self.padding(view, '.' + port['name'], 32)
                self.padding(view, '(' + port['name'] + port['dim'], 28)
            else:
                self.padding(view, '.' + port['name'], 32)
                self.padding(view, '(' + port['name'], 28)
            if i != len(ports_info) - 1:
                self.buf_print(view, '),')
            else:
                self.buf_print(view, ') ')
            self.print_dir_type(view, port['dir_type'])
        view.run_command('unindent')
        self.buf_print(view, ');\n')
        self.buf_print(view, '\n'*5)

    def padding(self, view, x, y):
        self.buf_print(view, x + ' '*(y-len(x)))

    def buf_print(self, view, str):
        view.run_command('insert', {'characters': str})

    def print_dir_type(self, view, dir_type):
        if dir_type == 'input':
            self.buf_print(view, '  // I\n')
        elif dir_type == 'output':
            self.buf_print(view, '  // O\n')
        elif dir_type == 'inout':
            self.buf_print(view, '  // B\n')
        else:
            self.buf_print(view, '  // ' + dir_type + '\n')