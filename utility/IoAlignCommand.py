import sublime, sublime_plugin
import re

pin = 31
net = 27
ps = r'(input|output|inout|reg|wire)\b\s*(\[.*\]){0,1}\s*\b([\w]+)\b'

class IoAlignCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        v = self.view
        p = re.compile(ps)
        # rs = v.find_all(ps, 0)
        start = 0
        while True:
            r = v.find(ps, start)
            if r.a < start:
                break
            s = v.substr(r)
            m = p.match(s)
            if m:
                if m.group(2) == None:
                    ns  = m.group(1) + " "*(8 - len(m.group(1)))
                else:
                    ns  = m.group(1) + " "*(8 - len(m.group(1))) + m.group(2)
                ns += " "*(32 - len(ns))
                ns += m.group(3)
            else:
                ns = s
            print(ns)
            v.replace(edit, r, ns)
            start=r.b
