import os
import sublime, sublime_plugin
from uvmf.generate import setTitle
from uvmf.generate import genAgtStr
from uvmf.generate import genAgts

def set_file_header(view):
    TM_AUTHOR = view.settings().get("author", "<author>")
    TM_EMAIL = view.settings().get("e-mail", "<e-mail>")
    TM_COPYRIGHT = view.settings().get("copyright", "<copyright>")
    setTitle("", TM_AUTHOR, TM_EMAIL, TM_COPYRIGHT)

class GenUvmEnvCommand(sublime_plugin.WindowCommand):
    def run(self):
        self.agt_name_list = []
        set_file_header(self.window.active_view())

        prj_file = self.window.project_file_name()
        if prj_file:
            self.prj_path = os.path.dirname(os.path.normpath(os.path.abspath(prj_file)))
            self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)
        else:
            self.window.show_input_panel("Agent path:", "", self.path_on_done, None, None)

    def name_on_done(self, text):
        if len(text) == 0:
            genAgts(self.agt_name_list, self.prj_path)
        else:
            self.agt_name_list.append(text)
            self.path_on_done(self.prj_path)

    def path_on_done(self, text):
        self.prj_path = os.path.normpath(os.path.abspath(text))
        if os.path.exists(self.prj_path) and os.path.isdir(self.prj_path) :
            self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)
        else:
            try:
                os.mkdir(self.prj_path)
                self.window.show_input_panel("Agent name:", "", self.name_on_done, None, None)
            except Exception:
                print("make dir failure")
