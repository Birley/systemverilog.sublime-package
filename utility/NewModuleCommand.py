import sublime, sublime_plugin
import datetime

module_tpl = """//**************************************************************************//
// Company              :   ${TM_COPYRIGHT}
// Engineer             :   ${TM_AUTHOR}
// Create Date          :   ${TM_DATE}
// Design Name          :   $1
// Module Name          :   $1
// Target Device        :
// Tool Version         :
// Initial Version      :
// Additional Comments  :
// Email                :   ${TM_EMAIL}
// Tel                  :
// Description          :
//**************************************************************************//
// Revision             :
// Modify by            :
// Modify Date          :
// Modify Description   :
//**************************************************************************//

`timescale 1ns/10ps

module ${1:${TM_FILENAME/(.+)\..+|.*/$1/:title}} (

);

/****************************************************************************\\
                             Parameter/Define
\****************************************************************************/

/****************************************************************************\\
                              Input/Output
\****************************************************************************/

/****************************************************************************\\
                              Register/Wire
\****************************************************************************/

/****************************************************************************\\
                                Main Code
\****************************************************************************/
$0

endmodule
"""

class NewModuleCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.TM_AUTHOR = self.view.settings().get("author", "<author>")
        self.TM_EMAIL = self.view.settings().get("e-mail", "<e-mail>")
        self.TM_COPYRIGHT = self.view.settings().get("copyright", "<copyright>")
        self.TM_DATE = datetime.datetime.now().strftime('%m/%d/%Y')
        self.TM_YEAR = datetime.datetime.now().strftime('%Y')
        self.view.run_command('insert_snippet', { 'contents': module_tpl, 'TM_YEAR': self.TM_YEAR,
            'TM_DATE': self.TM_DATE, 'TM_AUTHOR': self.TM_AUTHOR, 'TM_EMAIL': self.TM_EMAIL,
            'TM_COPYRIGHT': self.TM_COPYRIGHT})
        self.view.set_syntax_file("Packages/SystemVerilog/SystemVerilog.tmLanguage")
