import sublime, sublime_plugin
import re

pin = 31
net = 27
ps = r'^( *\.)(\w+)\s*\(\s*(.*)\)'

class PortAlignCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        v = self.view
        p = re.compile(ps)
        # rs = v.find_all(ps, 0)
        start = 0
        while True:
            r = v.find(ps, start)
            if r.a < start:
                break
            s = v.substr(r)
            m = p.match(s)
            if m:
                ns = " "*4 + "." + m.group(2)
                if (len(m.group(2)) < pin):
                    ns += " "*(pin - len(m.group(2)))
                ns += "("+m.group(3)
                if (len(m.group(3)) < net):
                    ns += " "*(net - len(m.group(3)))
                ns += ")"
            else:
                ns = s
            print(ns)
            v.replace(edit, r, ns)
            start=r.b
