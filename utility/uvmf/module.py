class Module():

    def __init__(self, name, base, parent = None):
        self.name = name
        self.base = base
        self.parent = parent
        if parent != None:
            self.path = parent.path+'/'+name
        else:
            self.path = name
        self.sub_mod=[]
        self.objs=[]
        
    def add_sub_mod(self, sub_mod):
        self.sub_mod.extend(sub_mod)

    def bind_obj(self, obj):
        if self.base == "agt":
            self.objs.extend(obj)
        
    def get_sub_mod(self, base):
        for mod in self.sub_mod:
            if mod.base == base:
                return mod
            
    def get_obj(self, base):
        for obj in self.objs:
            if obj.base == base:
                return obj

    def output(self, level = 0):
        if level == 0:
            print("-------------------------------------")
            
        print("    "*level+self.name)
        
        if self.base == "agt":
            for obj in self.objs:
                print("    "*(level+1)+obj.name)
        
        for mod in self.sub_mod:
            mod.output(level+1)
        
        if level == 0:
            print("-------------------------------------")