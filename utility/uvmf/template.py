#!/usr/bin/env python
from textwrap import dedent
from string import Template

class UVMTemp(Template):
########################################################################################################################
# template for file header
########################################################################################################################
    __file_header_temp = """        //--------------------------------------------------------------------------------------------------
        // Title             : ${title}
        // Project           : ${prj_name}
        //--------------------------------------------------------------------------------------------------
        // File              : ${title}.${ext}
        // Author            : ${author} (${email})
        // Platform          : ${platform}
        //--------------------------------------------------------------------------------------------------
        // Description       : ${desc}
        //--------------------------------------------------------------------------------------------------
        // ${copyright}
        //--------------------------------------------------------------------------------------------------
        // Revisions         :
        // Date         Version     Author          Description
        // ${date}     1.0       ${author}
        //--------------------------------------------------------------------------------------------------
        """

########################################################################################################################
# template for sequence item
########################################################################################################################
    __seq_item_temp = """
        `ifndef ${SEQ_ITEM}_SV_
        `define ${SEQ_ITEM}_SV_
        
        class ${seq_item} extends uvm_sequence_item;
        
            `uvm_object_utils_begin(${seq_item})
            `uvm_object_utils_end
        
            function new(string name = "${seq_item}");
                super.new(name);
            endfunction : new
        
        endclass : ${seq_item}
        
        `endif // ${SEQ_ITEM}_SV_
        """
########################################################################################################################
# template for sequencer
########################################################################################################################
    __sqr_temp = """
        `ifndef ${SQR}_SV_
        `define ${SQR}_SV_
        
        class ${sqr} extends uvm_sequencer#(${seq_item}, ${rsp_item});
        
            `uvm_sequencer_utils(${sqr})
        
            function new(string name = "${sqr}", uvm_component parent = null);
                super.new(name, parent);
            endfunction : new
        
        endclass : ${sqr}
        
        `endif // ${SQR}_SV_
        """

########################################################################################################################
# template for sequence library
########################################################################################################################
    __seq_temp = """
        `ifndef ${SEQ}_LIB_SV_
        `define ${SEQ}_LIB_SV_
        
        class ${seq}_base extends uvm_sequence#(${seq_item}, ${rsp_item});
        
            `uvm_sequence_utils(${seq}_base, ${sqr})
        
            function new(string name = "${seq}_base");
                super.new(name);
            endfunction : new
        
            virtual task body();
                super.body();
            endtask : body
        
        endclass : ${seq}_base
        
        `endif // ${SEQ}_LIB_SV_
        """

########################################################################################################################
# template for driver
########################################################################################################################
    __drv_temp = """
        `ifndef ${DRV}_SV_
        `define ${DRV}_SV_
        
        class ${drv} extends uvm_driver#(${seq_item}, ${rsp_item});
            virtual interface ${intf}  ${vintf};
            `uvm_component_utils(${drv})
        
            function new(string name = "${drv}", uvm_component parent = null);
                super.new(name, parent);
            endfunction : new
        
            virtual function void build_phase(uvm_phase phase);
                super.build_phase(phase);
            endfunction : build_phase
        
            virtual task main_phase(uvm_phase phase);
                super.main_phase(phase);
            endtask : main_phase
        
        endclass : ${drv}
        
        `endif // ${DRV}_SV_
        """

########################################################################################################################
# template for monitor
########################################################################################################################
    __mon_temp = """
        `ifndef ${MON}_SV_
        `define ${MON}_SV_
        
        class ${mon} extends uvm_monitor;
            // virtual interface ${intf}  ${vintf};
            `uvm_component_utils(${mon})
        
            function new(string name = "${mon}", uvm_component parent = null);
                super.new(name, parent);
            endfunction : new
        
            virtual function void build_phase(uvm_phase phase);
                super.build_phase(phase);
            endfunction : build_phase
        
            virtual task main_phase(uvm_phase phase);
                super.main_phase(phase);
            endtask : main_phase
        
        endclass : ${mon}
        
        `endif // ${MON}_SV_
        """

########################################################################################################################
# template for agent_cfg
########################################################################################################################
    __cfg_temp = """
        `ifndef ${CFG}_SV_
        `define ${CFG}_SV_
                
        class ${cfg} extends uvm_object;
          
            `uvm_object_utils(${cfg})

            // agent configuration
            uvm_active_passive_enum is_active = UVM_ACTIVE;   

            // virtual interface handle:
            virtual interface ${intf}  ${vintf};

            function new(string name = "${cfg}");
                super.new(name);
            endfunction : new

        endclass: ${cfg}

        `endif // ${CFG}_SV_
        """

########################################################################################################################
# template for interface
########################################################################################################################
    __intf_temp = """
        `timescale  1ns/10ps

        `ifndef ${INTF}_SV__
        `define ${INTF}_SV__

        interface ${intf}(input clk, rst_n);

            default clocking cb @(posedge clk);
                default input #0.5ns output #0.5ns;
                //input   ;
                //output  ;
                //inout   ;
            endclocking

        endinterface : ${intf}

        `endif  // ${INTF}_SV__
    """

########################################################################################################################
# template for agent
########################################################################################################################
    __agt_temp = """
        `ifndef ${AGT}_SV_
        `define ${AGT}_SV_
        
        `include "./${path}/${cfg}.sv"
        `include "./${path}/${seq_item}.sv"
        `include "./${path}/${sqr}.sv"
        `include "./${path}/${seq}.sv"
        `include "./${path}/${drv}.sv"
        `include "./${path}/${mon}.sv"
        
        class ${agt} extends uvm_agent;
            ${sqr}     u_${sqr};
            ${drv}     u_${drv};
            ${mon}     u_${mon};
            ${cfg}     cfg;

            `uvm_component_utils(${agt})
        
            function new(string name = "${agt}", uvm_component parent = null);
                super.new(name, parent);
            endfunction : new
        
            function void build_phase(uvm_phase phase);
                super.build_phase(phase);

                if(!uvm_config_db #(${cfg})::get(this, "", "${cfg}", cfg)) begin
                    `uvm_error("build_phase", "${cfg} not found")
                end
        
                if (cfg.is_active == UVM_ACTIVE) begin
                    u_${sqr} = ${sqr}::type_id::create("u_${sqr}", this);
                    u_${drv} = ${drv}::type_id::create("u_${drv}", this);
                end
        
                u_${mon} = ${mon}::type_id::create("u_${mon}", this);
            endfunction : build_phase
        
            function void connect_phase(uvm_phase phase);
                super.connect_phase(phase);
                if (cfg.is_active == UVM_ACTIVE) begin
                    u_${drv}.seq_item_port.connect(u_${sqr}.seq_item_export);
                    u_${drv}.${vintf} = cfg.${vintf};
                end
                // u_${mon}.${vintf} = cfg.${vintf};
            endfunction : connect_phase
        
        endclass : ${agt}
        
        `endif // ${AGT}_SV_
        """

########################################################################################################################
# template for env
########################################################################################################################
    __env_temp = """
        `ifndef ENV_SV__
        `define ENV_SV__

        class env_base extends uvm_env;
            `uvm_component_utils(env_base)

            ${agts_decl}
            scbd                    u_scbd;

            function new(string name = "env", uvm_component parent = null);
                super.new(name, parent);
            endfunction

            function void build_phase(uvm_phase phase);
                super.build_phase(phase);
                ${agts_inst}
                u_scbd = scbd::type_id::create("u_scbd", this);
            endfunction
            
            function void connect_phase(uvm_phase phase);
                super.connect_phase(phase);
                ${scbd_cnnt}
            endfunction

        endclass : env_base

        `endif
        """

########################################################################################################################
# template for test
########################################################################################################################
    __test_temp = """
        `ifndef TEST_SV_
        `define TEST_SV_

        class test_base extends uvm_test;
            `uvm_component_utils(test_base)
            
            env_base    u_env_base;
            
            function new(string name = "test_base", uvm_component parent = null);
                super.new(name, parent);
            endfunction
            
            function void build_phase(uvm_phase phase);
                super.build_phase(phase);
                u_env_base = env_base::type_id::create("u_env_base", this);
            endfunction

            task reset_phase(uvm_phase phase);
                super.reset_phase(phase);
                phase.raise_objection(this);
                print();
                #200ns;
                phase.drop_objection(this);
            endtask

        endclass : test_base

        `endif // TEST_SV_
    """

########################################################################################################################
# template for scoreboard
########################################################################################################################
    __scbd_temp = """
        `ifndef SCBD_SV__
        `define SCBD_SV__

        class scbd extends uvm_scoreboard;
            `uvm_component_utils(scbd)

            function new(string name = "scbd", uvm_component parent = null);
                super.new(name, parent);
            endfunction

            function void build_phase(uvm_phase phase);
                super.build_phase(phase);
            endfunction

        endclass : scbd

        `endif // SCBD_SV__
    """

########################################################################################################################
# template for sim_pkg
########################################################################################################################
    __pkg_temp = """
        `ifndef SIM_PKG_SV__
        `define SIM_PKG_SV__

        `ifndef DVT
        `include "uvm_macros.svh"
        `endif

        package sim_pkg;
            import uvm_pkg::*;

            ${agt_file}
            `include "scbd.sv"
            `include "seq_lib.sv"
            `include "env.sv"
            `include "test.sv"

        endpackage : sim_pkg

        `endif // SIM_PKG_SV__
    """

########################################################################################################################
# template for sim_top
########################################################################################################################
    __sim_top_temp = """
        `ifndef DVT
        `include "uvm_macros.svh"
        `endif

        `include "sim_pkg.sv"

        `timescale 1ns/10ps

        module sim_top (

        );

        import uvm_pkg::*;
        import sim_pkg::*;
        // ---------------------------------------------------------------------------
        //                            Parameter/Define
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        //                               Input/Output
        // ---------------------------------------------------------------------------

        // ---------------------------------------------------------------------------
        //                              Register/Wire
        // ---------------------------------------------------------------------------
        reg                             clk;
        reg                             rst_n;

        // ---------------------------------------------------------------------------
        //                                Main Code
        // ---------------------------------------------------------------------------

        ${intf_decl}
        // ---------------------------------------------------------------------------
        // Clock & Reset
        // ---------------------------------------------------------------------------
        initial begin
            clk = 1'b0;
            fork
                forever #3.2 clk = ~clk;
            join
        end

        initial begin
            rst_n = 1'b0;
            #123 rst_n = 1'b1;
        end

        // ---------------------------------------------------------------------------
        // UVM
        // ---------------------------------------------------------------------------
        ${cfg_decl}

        initial begin
            ${cfg_inst}
            ${vif_cnnt}
            ${cfg_set}

            run_test();
        end

        // ---------------------------------------------------------------------------
        // Dump Wave
        // ---------------------------------------------------------------------------
        initial begin
            $fsdbDumpfile("sim_top.fsdb");
            $fsdbDumpvars(0, "+mda", "sim_top");
        end

        endmodule
    """
########################################################################################################################
# template for sim_pkg
########################################################################################################################
    __lib_temp = """
        `ifndef SEQ_LIB_SV_
        `define SEQ_LIB_SV_


        `endif // SEQ_LIB_SV_
    """

########################################################################################################################
# common template
########################################################################################################################
    __decl_temp = """${type_name}    u_${type_name};"""

    __intf_decl_temp = """${type_name}    u_${type_name}(clk, rst_n);"""

    __inst_temp = """u_${type_name} = ${type_name}::type_id::create("u_${type_name}", this);"""

    __obj_temp = """u_${type_name} = ${type_name}::type_id::create("u_${type_name}");"""

    __scbd_cnnt_temp = """u_${type_name}_agt.u_${type_name}_drv.drv_scbd_port.connect(u_scbd.${type_name}_drv_imp);"""

    __vif_cnnt_temp = """u_${agt_name}_cfg.${agt_name}_vif = u_${agt_name}_if;"""

    __cfg_set_temp = """uvm_config_db#(${agt_name}_cfg)::set(uvm_root::get(), "*u_${agt_name}_agt*", "${agt_name}_cfg", u_${agt_name}_cfg);"""
########################################################################################################################

    __temp_dict = {"file_header":__file_header_temp,
                   "seq_item":__seq_item_temp,
                   "sqr":__sqr_temp,
                   "seq":__seq_temp,
                   "drv":__drv_temp,
                   "mon":__mon_temp,
                   "agt":__agt_temp,
                   "cfg":__cfg_temp,
                   "intf":__intf_temp,
                   "env":__env_temp,
                   "decl":__decl_temp,
                   "intf_decl":__intf_decl_temp,
                   "inst":__inst_temp,
                   "obj":__obj_temp,
                   "scbd_cnnt":__scbd_cnnt_temp,
                   "vif_cnnt":__vif_cnnt_temp,
                   "cfg_set":__cfg_set_temp,
                   "test":__test_temp,
                   "scbd":__scbd_temp,
                   "pkg":__pkg_temp,
                   "sim_top":__sim_top_temp,
                   "lib":__lib_temp}
    
    def __init__(self, temp = None):
        if temp == None:
            pass
        else:
            try:
                self.template = dedent(self.__temp_dict[temp])
            except KeyError:
                self.template = "NULL Template"
                print('Template "{0}" not found!')

    def file_header(self):
        return Template(dedent(self.__file_header_temp))
    
    def seq_item(self):
        return Template(dedent(self.__seq_item_temp))
    
    def sqr(self):
        return Template(dedent(self.__sqr_temp))
    
    def seq(self):
        return Template(dedent(self.__seq_temp))
    
    def drv(self):
        return Template(dedent(self.__drv_temp))
    
    def mon(self):
        return Template(dedent(self.__mon_temp))
    
    def agt(self):
        return Template(dedent(self.__agt_temp))

    def env(self):
        return Template(dedent(self.__env_temp))

    def decl(self):
        return Template(dedent(self.__decl_temp))

    def inst(self):
        return Template(dedent(self.__inst_temp))

    def scbd_cnnt(self):
        return Template(dedent(self.__scbd_cnnt_temp))

    def test(self):
        return Template(dedent(self.__test_temp))

    def scbd(self):
        return Template(dedent(self.__scbd_temp))

    def pkg(self):
        return Template(dedent(self.__pkg_temp))

    def lib(self):
        return Template(dedent(self.__lib_temp))
