from string import Template

port_tmp = """
//****************************************************************************
// Merge these into Driver
//****************************************************************************
class ${name}_drv extends uvm_driver#(${name}_item, ${name}_item);

    uvm_analysis_port#(${drv_trans})  drv_scbd_port;

    virtual function void build_phase(uvm_phase phase);
        drv_scbd_port = new("drv_scbd_port", this);
    endfunction : build_phase

endclass : ${name}_drv


//****************************************************************************
// Merge these into Monitor
//****************************************************************************
class ${name}_mon extends uvm_monitor;

    uvm_analysis_port#(${mon_trans})  mon_scbd_port;

    virtual function void build_phase(uvm_phase phase);
        mon_scbd_port = new("mon_scbd_port", this);
    endfunction : build_phase

endclass : ${name}_mon


//****************************************************************************
// Merge these into Scoreboard
//****************************************************************************
`uvm_analysis_imp_decl(_${name}_drv)
`uvm_analysis_imp_decl(_${name}_mon)

class scbd extends uvm_scoreboard;
    
    uvm_analysis_imp_${name}_drv#(${drv_trans}, scbd) ${name}_drv_imp;
    uvm_analysis_imp_${name}_mon#(${mon_trans}, scbd) ${name}_mon_imp;

    function void build_phase(input uvm_phase phase);
        ${name}_drv_imp = new("${name}_drv_imp", this);
        ${name}_mon_imp = new("${name}_mon_imp", this);
    endfunction

    task write_${name}_drv(input ${drv_trans} trans);
        `uvm_info("WR_SCBD", "write scoreboard, task 'write_${name}_drv'", UVM_MEDIUM)
    endtask : write_${name}_drv

    task write_${name}_mon(input ${mon_trans} trans);
        `uvm_info("WR_SCBD", "write scoreboard, task 'write_${name}_mon'", UVM_MEDIUM)
    endtask : write_${name}_mon

endclass : scbd


//****************************************************************************
// Merge these into ENV
//****************************************************************************
class env_base extends uvm_env;

    function void connect_phase(input uvm_phase phase);
        u_${name}_agt.u_${name}_drv.drv_scbd_port.connect(u_scoreboard.${name}_drv_imp);
        u_${name}_agt.u_${name}_mon.mon_scbd_port.connect(u_scoreboard.${name}_mon_imp);
    endfunction

endclass : env_base
"""

def gen_port(name, drv_trans, mon_trans):
    return Template(port_tmp).substitute({"name": name, "drv_trans": drv_trans, "mon_trans": mon_trans})
