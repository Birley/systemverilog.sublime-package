from uvmf.template import UVMTemp
from uvmf.module import Module
from uvmf.object import Object
from os.path import os
from datetime import date

hdr_dict = {"title":'',
            "prj_name":'',
            "ext":'sv',
            "author":'',
            "email":'',
            "platform":'',
            "desc":'',
            "copyright":'',
            "date":date.today().isoformat()}

def setTitle(prj_name, author, email, copyright):
    hdr_dict['prj_name'] = prj_name
    hdr_dict['author'] = author
    hdr_dict['email'] = email
    hdr_dict['copyright'] = copyright

def genFile(file_path, name, cntxt):
    f = open(os.path.join(file_path, name+'.sv'), "w", encoding="utf-8")
    f.write(cntxt)
    f.close()

def genAgtStr(name, file_path):
    agt = Module(name+"_agt", "agt")

    sqr = Module(name+"_sqr", "sqr", agt)
    drv = Module(name+"_drv", "drv", agt)
    mon = Module(name+"_mon", "mon", agt)

    agt_cfg = Object(name+"_cfg", "cfg")
    seq_item = Object(name+"_item", "seq_item")
    seq = Object(name+"_seq", "seq")

    intf = Object(name+"_if", "intf")
    vintf = Object(name+"_vif", "vintf")

    agt.add_sub_mod([sqr, drv, mon])
    agt.bind_obj([agt_cfg, seq_item, seq, intf, vintf])

    genAgt(agt, file_path)

    return agt


def genAgt(agt, file_path):
    cfg = agt.get_obj("cfg")
    seq_item = agt.get_obj("seq_item")
    rsp_item = agt.get_obj("seq_item")
    seq = agt.get_obj("seq")
    sqr = agt.get_sub_mod("sqr")
    drv = agt.get_sub_mod("drv")
    mon = agt.get_sub_mod("mon")
    intf = agt.get_obj("intf")
    vintf = agt.get_obj("vintf")

    sub_dict = {"cfg":cfg.name.lower(),
                "seq_item":seq_item.name.lower(),
                "rsp_item":rsp_item.name.lower(),
                "seq":seq.name.lower(),
                "sqr":sqr.name.lower(),
                "drv":drv.name.lower(),
                "mon":mon.name.lower(),
                "agt":agt.name.lower(),
                "CFG":cfg.name.upper(),
                "SEQ_ITEM":seq_item.name.upper(),
                "RSP_ITEM":rsp_item.name.upper(),
                "SEQ":seq.name.upper(),
                "SQR":sqr.name.upper(),
                "DRV":drv.name.upper(),
                "MON":mon.name.upper(),
                "AGT":agt.name.upper(),
                "path":agt.name.lower(),
                "intf":intf.name.lower(),
                "INTF":intf.name.upper(),
                "vintf":vintf.name.lower()}

    cntxt = {"cfg":[cfg, UVMTemp("cfg").safe_substitute(sub_dict)],
            "intf":[intf, UVMTemp("intf").safe_substitute(sub_dict)],
            "seq_item":[seq_item, UVMTemp("seq_item").safe_substitute(sub_dict)],
            "seq":[seq, UVMTemp("seq").safe_substitute(sub_dict)],
            "sqr":[sqr, UVMTemp("sqr").safe_substitute(sub_dict)],
            "drv":[drv, UVMTemp("drv").safe_substitute(sub_dict)],
            "mon":[mon, UVMTemp("mon").safe_substitute(sub_dict)],
            "agt":[agt, UVMTemp("agt").safe_substitute(sub_dict)]}

    # if os.path.exists(sub_dict["path"]):
    #     pass
    # else:
    #     os.mkdir(os.getcwd()+'/'+sub_dict["path"])

    for v in cntxt.values():
        hdr_dict["title"] = v[0].name
        hdr = UVMTemp("file_header").safe_substitute(hdr_dict)
        genFile(file_path, v[0].name, hdr + v[1])

def genAgtComp(name, comp_type):
    agt = Module(name+"_agt", "agt")

    sqr = Module(name+"_sqr", "sqr", agt)
    drv = Module(name+"_drv", "drv", agt)
    mon = Module(name+"_mon", "mon", agt)

    cfg = Object(name+"_cfg", "cfg")
    seq_item = Object(name+"_item", "seq_item")
    rsp_item = seq_item
    seq = Object(name+"_seq", "seq")

    intf = Object(name+"_if", "intf")
    vintf = Object(name+"_vif", "vintf")

    sub_dict = {"cfg":cfg.name.lower(),
                "seq_item":seq_item.name.lower(),
                "rsp_item":rsp_item.name.lower(),
                "seq":seq.name.lower(),
                "sqr":sqr.name.lower(),
                "drv":drv.name.lower(),
                "mon":mon.name.lower(),
                "agt":agt.name.lower(),
                "CFG":cfg.name.upper(),
                "SEQ_ITEM":seq_item.name.upper(),
                "RSP_ITEM":rsp_item.name.upper(),
                "SEQ":seq.name.upper(),
                "SQR":sqr.name.upper(),
                "DRV":drv.name.upper(),
                "MON":mon.name.upper(),
                "AGT":agt.name.upper(),
                "path":agt.name.lower(),
                "intf":intf.name.lower(),
                "vintf":vintf.name.lower()}
    return UVMTemp("file_header").safe_substitute(hdr_dict) + UVMTemp(comp_type).safe_substitute(sub_dict)

def genAgts(agt_name_list, prj_path):
    agt_list = []
    for agt_name in agt_name_list:
        agt_path = os.path.join(prj_path, agt_name) + "_agt"
        if os.path.exists(agt_path) and os.path.isdir(agt_path):
            pass
        else:
            try:
                os.mkdir(agt_path)
            except Exception:
                print("make dir failure")
        agt_list.append(genAgtStr(agt_name, agt_path))
    genTb(agt_list, prj_path)

def genTb(agt_list, prj_path):
    genEnv(agt_list, prj_path)
    genTest(prj_path)
    genScbd(prj_path)
    genLib(prj_path)
    genPkg(agt_list, prj_path)
    genSimTop(agt_list, prj_path)

def genEnv(agt_list, prj_path):
    decl_str = ""
    inst_str = ""
    scbd_cnnt_str = ""

    for agt in agt_list:
        decl_str = decl_str + UVMTemp("decl").safe_substitute({"type_name":agt.name.lower()}) + "\n" + " "*4
        inst_str = inst_str + UVMTemp("inst").safe_substitute({"type_name":agt.name.lower()}) + "\n" + " "*8
        # scbd_cnnt_str = scbd_cnnt_str + UVMTemp("scbd_cnnt").safe_substitute({"type_name":agt.name.lower()[0:-4]}) + "\n" + " "*8

    print(decl_str)
    print(inst_str)
    print(scbd_cnnt_str)

    env_str = UVMTemp("env").safe_substitute({"agts_decl":decl_str,
                                              "agts_inst":inst_str,
                                              "scbd_cnnt":scbd_cnnt_str})
    hdr_dict["title"] = "env"
    hdr = UVMTemp("file_header").safe_substitute(hdr_dict)

    print(env_str)
    genFile(prj_path, "env", hdr + env_str)

def genTest(prj_path):
    hdr_dict["title"] = "test"
    hdr = UVMTemp("file_header").safe_substitute(hdr_dict)
    test_str = UVMTemp("test").safe_substitute(hdr_dict)
    genFile(prj_path, "test", hdr + test_str)

def genScbd(prj_path):
    hdr_dict["title"] = "scbd"
    hdr = UVMTemp("file_header").safe_substitute(hdr_dict)
    scbd_str = UVMTemp("scbd").safe_substitute(hdr_dict)
    genFile(prj_path, "scbd", hdr + scbd_str)

def genLib(prj_path):
    hdr_dict["title"] = "seq_lib"
    hdr = UVMTemp("file_header").safe_substitute(hdr_dict)
    lib_str = UVMTemp("lib").safe_substitute(hdr_dict)
    genFile(prj_path, "seq_lib", hdr + lib_str)

def genPkg(agt_list, prj_path):
    agt_file_str = ""

    for agt in agt_list:
        agt_file_str = agt_file_str + "`include " + "\"" + agt.name.lower() + "/" + agt.name.lower() + ".sv\"\n" + " "*4

    pkg_str = UVMTemp("pkg").safe_substitute({"agt_file":agt_file_str})

    hdr_dict["title"] = "sim_pkg"
    hdr = UVMTemp("file_header").safe_substitute(hdr_dict)

    genFile(prj_path, "sim_pkg", hdr + pkg_str)

def genSimTop(agt_list, prj_path):
    intf_decl_str = ""
    cfg_decl_str = ""
    cfg_inst_str = ""
    vif_cnnt_str = ""
    cfg_set_str = ""

    for agt in agt_list:
        intf_decl_str = intf_decl_str + UVMTemp("intf_decl").safe_substitute({"type_name":agt.name.lower()[0:-4] + "_if"}) + "\n"
        cfg_decl_str = cfg_decl_str + UVMTemp("decl").safe_substitute({"type_name":agt.name.lower()[0:-4] + "_cfg"}) + "\n"
        cfg_inst_str = cfg_inst_str + UVMTemp("obj").safe_substitute({"type_name":agt.name.lower()[0:-4] + "_cfg"}) + "\n" + " "*4
        vif_cnnt_str = vif_cnnt_str + UVMTemp("vif_cnnt").safe_substitute({"agt_name":agt.name.lower()[0:-4]}) + "\n" + " "*4
        cfg_set_str = cfg_set_str + UVMTemp("cfg_set").safe_substitute({"agt_name":agt.name.lower()[0:-4]}) + "\n" + " "*4

    sim_top_str = UVMTemp("sim_top").safe_substitute({"intf_decl":intf_decl_str,
                                                     "cfg_decl":cfg_decl_str,
                                                     "cfg_inst":cfg_inst_str,
                                                     "vif_cnnt":vif_cnnt_str,
                                                     "cfg_set":cfg_set_str})
    hdr_dict["title"] = "sim_top"
    hdr = UVMTemp("file_header").safe_substitute(hdr_dict)

    genFile(prj_path, "sim_top", hdr + sim_top_str)
